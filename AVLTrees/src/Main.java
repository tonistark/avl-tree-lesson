import java.util.ArrayList;
import java.util.function.Consumer;

public class Main {

    public static void main(String[] args) {
        AVLTree<Integer> tree = new AVLTree<>();
        for (int i = 0; i < 20; i++) {
            tree.insert(i);
        }
        for (int i = 1; i < 20; i += 2) {
            tree.remove(i);
        }
        tree.printDebug();
    }

}















/**
 * An AVL tree is a self-balancing binary tree with the condition that two
 * adjacent child nodes can differ in height of at most 1.
 *
 * @author Toni-Tran
 * @param <T>
 */
class AVLTree<T extends Comparable<T>> {

    public static enum Order {

        PRE_ORDER, IN_ORDER, POST_ORDER, BREADTH_ORDER
    };

    private AVLNode root;
    private int size;



    public AVLTree() {
        size = 0;
    }



    /**
     * Inserts a piece of data into the BST. Relies upon a recursive helper. It
     * attempts to "replace" the root node with a modified version of itself, if
     * any modifications occurred.
     *
     * @param data
     *            The data to insert.
     */
    public void insert(T data) {
        /* Check for attempted null insertion. */
        if (data == null) {
            return;
        }
        root = insertHelper(root, data);
    }



    /**
     * Private helper method for the insertion method. Returns a modified
     * version of the passed in node.
     *
     * @param node
     *            The node to examine and modify for return.
     * @param data
     *            The data to be inserted in the correct spot, when found.
     * @return a modified version of the root and its sub-trees.
     */
    private AVLNode insertHelper(AVLNode node, T data) {
        /*
         * We have found a null location where we are supposed to insert a new
         * node.
         */
        if (node == null) {
            AVLNode n = new AVLNode();
            n.data = data;
            size++;
            return n;
        }
        if (node.data.compareTo(data) == 0) {
            /*
             * If we have found a node which already contains that particular
             * piece of data, return an unmodified version of itself to the
             * parent stack, one level above the current recursive stack.
             */
            return node;
        } else if (data.compareTo(node.data) < 0) {
            /*
             * If the place to insert is somewhere in the left sub-tree, recurse
             * to the left.
             */
            node.leftChild = insertHelper(node.leftChild, data);
            /* Check for an imbalance after insertion. */
            if (heightOf(node.leftChild) - heightOf(node.rightChild) == 2) {
                /*
                 * Check to see if we have an imbalanced left-left (zig-zig), or
                 * left-right (zig-zag).
                 */
                if (data.compareTo(node.leftChild.data) < 0) {
                    /* We have a left-left. Perform a single left rotation. */
                    node = singleRightRotateWithLeftChild(node);
                } else {
                    /* Else, we have a left-right. */
                    node = doubleRotateWithLeftChild(node);
                }
            }
        } else {
            /* Else, the only place left is to recurse to the right. */
            node.rightChild = insertHelper(node.rightChild, data);
            /* Check for an imbalance after insertion. */
            if (heightOf(node.rightChild) - heightOf(node.leftChild) == 2) {
                /*
                 * Check to see if we have an imbalanced left-left (zig-zig), or
                 * left-right (zig-zag).
                 */
                if (data.compareTo(node.rightChild.data) > 0) {
                    /* We have a right-right. Perform a single right rotation. */
                    node = singleLeftRotateWithRightChild(node);
                } else {
                    /* Else, we have a right-left. */
                    node = doubleRotateWithRightChild(node);
                }
            }
        }
        /* Return the now modified sub-tree. */
        return node;
    }



    /**
     * Removes a piece of data from the BST. Relies upon a recursive helper. It
     * attempts to "replace" the root node with a modified version of itself, if
     * any modifications occurred.
     *
     * @param data
     *            The data to remove.
     */
    public void remove(T data) {
        if (data == null) {
            return;
        }
        root = removeHelper(root, data);
    }



    /**
     * Private helper method for the remove method. Returns a modified version
     * of the passed in node.
     *
     * @param node
     *            The node to examine and modify for return.
     * @param data
     *            The data to be removed from the correct spot, when found.
     * @return a modified version of the root and its sub-trees.
     */
    private AVLNode removeHelper(AVLNode node, T data) {
        if (node.data == data) {
            /*
             * We have found the node we need to remove. Does it have any
             * children we need to take care of?
             */
            if (node.leftChild == null && node.rightChild == null) {
                /*
                 * The node we are about to delete has no children at all.
                 * Simply return a null to replace the subtree.
                 */
                size--;
                return null;
            } else if (node.leftChild != null && node.rightChild != null) {
                /*
                 * The node we are about to delete has 2 children. First,
                 * replace the current node's data with the minimum of the right
                 * subtree. Then, delete that minimum node we just found.
                 */
                node.data = findMin(node.rightChild);
                node.rightChild = removeHelper(node.rightChild, node.data);
            } else {
                /*
                 * The node we are about to delete has 1 child. Return that
                 * child to replace the subtree. Note that I am using a ternary
                 * operation. It follows the format of (Condition)? returnIfTrue
                 * : returnIfFalse.
                 */
                size--;
                return (node.leftChild != null) ? node.leftChild
                        : node.rightChild;
            }
        } else if (data.compareTo(node.data) < 0) {
            /* We need to search the left sub-tree to find the node to remove. */
            node.leftChild = removeHelper(node.leftChild, data);
        } else {
            /* We need to search the right sub-tree to find the node to remove. */
            node.rightChild = removeHelper(node.rightChild, data);
        }
        /* Return the now modified sub-tree. */
        return node;
    }



    /**
     * Returns true if the BST is empty.
     *
     * @return true if the BST is empty.
     */
    public boolean isEmpty() {
        return size == 0;
    }



    /**
     * Returns the number of elements currently stored in the BST.
     *
     * @return the number of elements currently stored in the BST.
     */
    public int size() {
        return size;
    }



    /**
     * Finds the node with the minimum value located within a particular subtree
     * and returns its data.
     *
     * @param node
     *            The node to search through for a min value.
     * @return the node with the minimum value located within a particular
     *         subtree.
     */
    public T findMin(AVLNode node) {
        if (node.leftChild == null) {
            /* The node itself is already the minimum. */
            return node.data;
        }
        /* Recurse down the left subtree, repeatedly until we find the min node. */
        return findMin(node.leftChild);
    }



    /**
     * Finds the node with the minimum value located within a particular subtree
     * and returns its data.
     *
     * @param node
     *            The node to search through for a max value.
     * @return the node with the maximum value located within a particular
     *         subtree.
     */
    public T findMax(AVLNode node) {
        if (node.rightChild == null) {
            /* The node itself is already the maximum. */
            return node.data;
        }
        /* Recurse down the left subtree, repeatedly until we find the min node. */
        return findMax(node.rightChild);
    }



    /**
     * Returns the height of a subtree.
     *
     * If the subtree has:
     * <ul>
     * <li>no children, the height is 0.</li>
     * <li>one child, the height is 1+the child's height.</li>
     * <li>two children, the height is 1+the taller child's height.</li>
     * </ul>
     *
     * @param leaf
     *            the subtree whose height we are trying to find.
     * @return the height of the subtree.
     */
    private int heightOf(AVLNode leaf) {
        if (leaf == null) {
            return 0;
        }
        int leftHeight = heightOf(leaf.leftChild);
        int rightHeight = heightOf(leaf.rightChild);
        return leftHeight > rightHeight ? 1 + leftHeight : 1 + rightHeight;
    }



    /**
     * Right-Right detected. Performs a single left rotation with the child
     * node.
     *
     * @param k1
     *            the child node.
     * @return the modified sub-tree.
     */
    private AVLNode singleLeftRotateWithRightChild(AVLNode k1) {
        AVLNode k2 = k1.rightChild;
        k1.rightChild = k2.leftChild;
        k2.leftChild = k1;
        return k2;
    }



    /**
     * Left-Left detected. Performs a single right rotation with the child node.
     *
     * @param k2
     *            the child node.
     * @return the modified sub-tree.
     */
    private AVLNode singleRightRotateWithLeftChild(AVLNode k2) {
        AVLNode k1 = k2.leftChild;
        k2.leftChild = k1.rightChild;
        k1.rightChild = k2;
        return k1;
    }



    /**
     * Right-Left detected. Performs a double rotation with the child node.
     *
     * @param k1
     *            the child node.
     * @return the modified sub-tree.
     */
    private AVLNode doubleRotateWithRightChild(AVLNode k1) {
        k1.leftChild = singleRightRotateWithLeftChild(k1.leftChild);
        return singleLeftRotateWithRightChild(k1);
    }



    /**
     * Left-Right detected. Performs a double rotation with the child node.
     *
     * @param k3
     *            the child node.
     * @return the modified sub-tree.
     */
    private AVLNode doubleRotateWithLeftChild(AVLNode k3) {
        k3.leftChild = singleLeftRotateWithRightChild(k3.leftChild);
        return singleRightRotateWithLeftChild(k3);
    }



    /**
     * Prints the tree in the order specified.
     *
     * @param o
     *            The order to print the tree in.
     */
    public void printTree(Order o) {
        /**
         * Passes in a consumer which will perform an action on each of the
         * nodes it passes by.
         */
        Consumer<T> nodePrinter = (Consumer<T>) (T t) -> {
            System.out.print(t + ", ");
        };
        traverse(o, nodePrinter);
    }



    /**
     * Traverses the tree in the provided order, and performs an action upon
     * each node. The action is determined by the supplied Consumer object.
     *
     * @param o
     *            The order to traverse the tree in.
     * @param consumer
     *            The consumer which will perform an action on each of the nodes
     *            data.
     */
    public void traverse(Order o, Consumer<T> consumer) {
        switch (o) {
        case PRE_ORDER:
            traversePreOrder(root, consumer);
            break;
        case IN_ORDER:
            traverseInOrder(root, consumer);
            break;
        case POST_ORDER:
            traversePostOrder(root, consumer);
            break;
        case BREADTH_ORDER:
            traverseBreadthOrder(consumer);
            break;
        }
    }



    /**
     * Traverse the tree, pre-order and performs the provided consumer's action
     * on each node.
     *
     * @param node
     *            The node currently examined.
     * @param consumer
     *            The consumer object which contains an action to be performed
     *            on the current node's data.
     */
    private void traversePreOrder(AVLNode node, Consumer<T> consumer) {
        if (node == null) {
            return;
        }
        consumer.accept(node.data);
        traversePreOrder(node.leftChild, consumer);
        traversePreOrder(node.rightChild, consumer);
    }



    /**
     * Traverse the tree, in-order and performs the provided consumer's action
     * on each node.
     *
     * @param node
     *            The node currently examined.
     * @param consumer
     *            The consumer object which contains an action to be performed
     *            on the current node's data.
     */
    private void traverseInOrder(AVLNode node, Consumer<T> consumer) {
        if (node == null) {
            return;
        }
        traverseInOrder(node.leftChild, consumer);
        consumer.accept(node.data);
        traverseInOrder(node.rightChild, consumer);
    }



    /**
     * Traverse the tree, post-order and performs the provided consumer's action
     * on each node.
     *
     * @param node
     *            The node currently examined.
     * @param consumer
     *            The consumer object which contains an action to be performed
     *            on the current node's data.
     */
    private void traversePostOrder(AVLNode node, Consumer<T> consumer) {
        if (node == null) {
            return;
        }
        traversePostOrder(node.leftChild, consumer);
        traversePostOrder(node.rightChild, consumer);
        consumer.accept(node.data);
    }



    /**
     * Traverse the tree, breadth-first, and performs the consumer's action on
     * each node.
     *
     * @param consumer
     *            The consumer which contains the action to be performed on each
     *            node this method traverses.
     */
    private void traverseBreadthOrder(Consumer<T> consumer) {
        /* Set up a queue for nodes. */
        ArrayList<AVLNode> queue = new ArrayList<>();
        /* Initiate with the root. */
        queue.add(root);
        int iterator = 0;
        AVLNode currentNode;
        while (iterator != queue.size()) {
            /* Set the current node to perform an action on. */
            currentNode = queue.get(iterator);
            consumer.accept(currentNode.data);
            /* If it has a left child, add it to the queue. */
            if (currentNode.leftChild != null) {
                queue.add(currentNode.leftChild);
            }
            /* If it has a right child, add it to the queue. */
            if (currentNode.rightChild != null) {
                queue.add(currentNode.rightChild);
            }
            /*
             * Increment the counter so we can continue down the queue until
             * there are no more nodes left.
             */
            iterator++;
        }
    }



    /**
     * Prints debug information about the tree.
     */
    public void printDebug() {
        System.out.println("Size: " + size + "\n==========");
        printDebugHelper(root);
    }



    /**
     * Helper method that prints the tree, as matter of fact, in pre-order.
     *
     * @param node
     *            The node to examine.
     */
    private void printDebugHelper(AVLNode node) {
        System.out.println("Data: " + node.data);
        System.out.println("Height: " + heightOf(node));
        System.out.println("Left: " + node.leftChild);
        System.out.println("Right: " + node.rightChild);
        System.out.println("----------");
        if (node.leftChild != null) {
            printDebugHelper(node.leftChild);
        }
        if (node.rightChild != null) {
            printDebugHelper(node.rightChild);
        }
    }

    class AVLNode {

        private T data;
        AVLNode leftChild;
        AVLNode rightChild;



        public AVLNode() {
            data = null;
            leftChild = null;
            rightChild = null;
        }



        @Override
        public String toString() {
            return data.toString();
        }
    }
}
